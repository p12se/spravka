package ru.mfc_omsk;

import ru.mfc_omsk.jdbf.DBFReader;

import java.nio.file.Path;

/**
 * Created by p12se on 02.06.14.
 */
public class TableFromDBF {

    public String createTableFromDBF(Path pathFileDbf, DBFReader dbfReader) {

        int lenghtArray = dbfReader.getFieldCount();

        String nameFile = pathFileDbf.getFileName().toString();

        int lengthName = nameFile.length() - 4;

        String stringQury = "";
        String arrayData[];
        String createTable = "CREATE TABLE " + nameFile.substring(0, lengthName) + " (\n";

        arrayData = new String[lenghtArray];

        for (int i = 0; i < dbfReader.getFieldCount(); i++) {

            switch (dbfReader.getField(i).getType()) {
                case 'C':
                    arrayData[i] = "\t" + dbfReader.getField(i) + " VARCHAR(" + dbfReader.getField(i).getLength() + ")";
                    break;
                case 'N':
                    arrayData[i] = "\t" + dbfReader.getField(i) + " INT";
                    break;
                case 'D':
                    arrayData[i] = "\t" + dbfReader.getField(i) + " datetime";
                    break;
            }
        }

        for (int i = 0; i < lenghtArray; i++) {
            if (i < (lenghtArray - 1)) {
                stringQury = stringQury + arrayData[i] + ", \n";
            } else {
                stringQury = stringQury + arrayData[i] + " )\n";
            }
        }
        return createTable + stringQury;
    }
}