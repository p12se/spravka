package ru.mfc_omsk;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectSQL {

    public Statement connectMySQLServer () throws ClassNotFoundException, SQLException {
        final String USERNAME = "root";
        final String PASSWORD = "";
        final String SQLSERVER = "jdbc:mysql://localhost/";
        final String DATABESE = "spravka??useUnicode=true&characterEncoding=UTF-8";

        Class.forName("com.mysql.jdbc.Driver");

        Connection connection = DriverManager.getConnection(SQLSERVER+DATABESE, USERNAME,PASSWORD);

        Statement statement = connection.createStatement();
        return statement;

    }

    public Statement connectMsServer() throws SQLException, ClassNotFoundException {

        final String USERNAME = "sa";
        final String PASSWORD = "";
        final String SQLSERVER = "";
        final String DATABESE = "spravka";

        String connect =
                        "jdbc:sqlserver://" + SQLSERVER +
                        ";user=" + USERNAME +
                        ";password=" + PASSWORD +
                        ";database=" + DATABESE;

        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        Connection conn = DriverManager.getConnection(connect);
        Statement sta = conn.createStatement();

        return sta;
    }
}
