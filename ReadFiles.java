package ru.mfc_omsk;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class ReadFiles {

    public DirectoryStream<Path> dirReadAccounts(Path dir) throws IOException {
        DirectoryStream<Path> stream = Files.newDirectoryStream(dir, "k_*.dbf");
        return stream;
    }
    public DirectoryStream<Path> dirReadTennats(Path dir) throws IOException {
        DirectoryStream<Path> stream = Files.newDirectoryStream(dir, "J_01.dbf");
        return stream;
    }

}
