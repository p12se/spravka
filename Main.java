package ru.mfc_omsk;

import ru.mfc_omsk.jdbf.DBFReader;
import ru.mfc_omsk.jdbf.JDBFException;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;

public class Main {

    public static void main(String[] args) throws IOException, JDBFException, ClassNotFoundException, SQLException, IllegalAccessException, InstantiationException {
        long start = System.currentTimeMillis();
        Path dir = Paths.get("./dbf").toAbsolutePath().normalize();

        DBFReader dbfReader = null;
        TableFromDBF tableFromDBF = new TableFromDBF();

        ReadFiles readFiles = new ReadFiles();
        SQLFromDBF sqlFromDBF = new SQLFromDBF();

        DirectoryStream<Path> d = readFiles.dirReadTennats(dir);
        for (Path t: d){
            Path pathFileDbf = Paths.get(String.valueOf(t));

            try {
                dbfReader = new DBFReader(pathFileDbf.toString());
            } catch (JDBFException e) {
                System.out.println(e + pathFileDbf.toString());
                return;
            }

            sqlFromDBF.parseToSQL(dbfReader);

        }

        long end = System.currentTimeMillis();
        long traceTime = end-start;
        System.out.println(traceTime/1000/60 + " min : " + traceTime/1000 + " sec" );
    }
}

