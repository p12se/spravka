package ru.mfc_omsk;


import ru.mfc_omsk.jdbf.DBFReader;
import ru.mfc_omsk.jdbf.JDBFException;

import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SQLFromDBF {

    public void parseToSQL(DBFReader dbfReader) throws JDBFException, IOException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {

        String insertInto = "INSERT INTO tennats values (";
        ConnectSQL connectSQL = new ConnectSQL();
        Statement ex = connectSQL.connectMySQLServer();

        SimpleDateFormat dFormat = new SimpleDateFormat("yyyy.MM.dd");

        ArrayList<String> dataArray = new ArrayList<String>();

        String table[] = new String[1000];
        int count=0;

        for (int i = 0; dbfReader.hasNextRecord(); i++) {
            int n = 0;
            String sql = insertInto;
            Object aobj[] = dbfReader.nextRecord(Charset.forName("cp866"));

            for (int j = 0; j < aobj.length; j++) {

                if (aobj[j] instanceof String) {

                    if (j < (aobj.length - 1)) {
                        sql = sql + "\'" + aobj[j] + "\', ";
                    } else {
                        sql = sql + "\'" + aobj[j] + "\'";
                    }
                } else if (aobj[j] instanceof Date) {
                    String date = dFormat.format(aobj[j]);
                    if (j < (aobj.length - 1)) {
                        sql = sql + "\'" + date + "\', ";
                    } else {
                        sql = sql + "\'" + date + "\'";
                    }
                } else {
                    if (j < (aobj.length - 1)) {
                        sql = sql + aobj[j] + ", ";
                    } else {
                        sql = sql + aobj[j].toString();
                    }
                }
            }

            sql = "" + sql + ");";

            if (count < 1000 & dbfReader.hasNextRecord()) {

                table[count] = sql;
                count++;
            } else {
                ex.executeUpdate("START TRANSACTION");
                for (String s: table){
                    ex.executeUpdate(s);
                }
                ex.executeUpdate("COMMIT");
                count=0;

            }

//            if (dbfReader.hasNextRecord() & i%1000 <999) {
//
//                dataArray.add(sql);
//            } else {
//                 ex.executeUpdate("START TRANSACTION");
//                 for (n=0; n<1000; n++){
//                     try {
//                         ex.executeUpdate(dataArray.get(n));
//                     } catch (IndexOutOfBoundsException e){
//                        continue;
//                     }
//                 }
//                ex.executeUpdate("COMMIT");
//                dataArray.clear();
        }
        ex.close();
    }
}




































